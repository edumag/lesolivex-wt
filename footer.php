	<?php 
		$footer_widgets_disabled = of_get_option( 'md_footer_widgets_disabled');
		$footer_widgets_columns = of_get_option( 'md_footer_widgets_columns');

		// $footer_widgets_disabled = FALSE;
		// $footer_widgets_columns = 4;
		
		if($footer_widgets_columns==4) { 
			$columnclass = "four columns";
		}elseif($footer_widgets_columns==3) {
			$columnclass = "one-third column";
		}elseif($footer_widgets_columns==2) {
			$columnclass = "eight columns";
		}elseif($footer_widgets_columns==1) {
			$columnclass = "sixteen columns";
		}


	?>
	<br class="clear" />

	</div>

	<footer>
	    <div class="container sixteen columns">
	        <hr class="footer border-color" />
	        <div id="footer_logos">
	            <a class="Linux" href="https://ca.wikipedia.org/wiki/Programari_lliure" title="Viquipèdia">
	                <img src="<?php echo content_url('/uploads/2015/01/linux.png');?>" alt="linux" />
	            </a>
	            <a class="monedasocial" href="http://lesolivex.com/servicio/tarifas">
	                <img alt="Moneda social" style="border-width:0"
	                    src="<?php echo content_url('/uploads/2015/01/ms-75x75.png');?>" />
	            </a>
	            <a class="ubuntu" href="http://www.ubuntu.com/" title="Programari lliure">
	                <img alt="Programari lliure" style="border-width:0"
	                    src="<?php echo content_url('/uploads/2015/01/Ubuntu_logo_orange-75px.png');?>" />
	            </a>
	            <a class="license" href="http://creativecommons.org/licenses/by/3.0/deed.es_CO">
	                <img alt="Licencia Creative Commons" style="border-width:0" src="<?php echo content_url('/uploads/2015/01/cc.png"');?> />
                      </a>

                    </div>
                    <hr class=" footer border-color" />
	                <?php 
						if($footer_widgets_disabled!=1) {	
					?>
	                <?php 
                        	for($i=1;$i <= $footer_widgets_columns; $i++) {
                        ?>
	                <div
	                    class="<?php echo $columnclass; if($i==1) echo ' alpha'; if($i==$footer_widgets_columns) echo ' omega'; ?>">
	                    <?php if(is_active_sidebar( 'bottom-' . $i)) { ?>
	                    <?php dynamic_sidebar( 'bottom-'.$i ); ?>
	                    <?php } ?>
	                </div>
	                <?php } ?>

	                <?php } ?>
	        </div>
	        <?php 
						if($footer_widgets_disabled!=1) {
					?>
	        <br class="clear" />
	        <div class="sixteen columns">
	            <hr class="border-color" />
	        </div>
	        <?php } ?>
	</footer>
	<div class="mobilemenu">
	    <?php if(!of_get_option('md_header_disable_search')) : ?>
	    <form>
	        <input type="text" class="medium" value=""><button type="submit"><i class='icon-search'></i></button>
	    </form>
	    <?php endif; ?>

	    <?php wp_nav_menu(array(
                        'theme_location' => 'main_menu',
                        'container' => '',
                        'menu_class' => 'mob-nav',
                        'before' => '',
                        'fallback_cb' => ''
                    ));
			 ?>
	</div>

	<a href="#" class="backtotop"></a>
	<div class="ajaxloader"><img src="<?php echo get_template_directory_uri();?>/images/loader.gif" /></div>
	<?php 
  
    // ADD ANALYTICS CODE
    echo of_get_option('md_footer_googleanalytics');

    // ADD SHARING SCRIPTS
    echo showshareingpost('','','',1);
    
  ?>


	<?php
  get_template_part( 'admin', 'custom' );
  wp_footer();
  ?>

	  </body>
	</html>