<?php 

	if(have_posts()) { 
		the_post(); 
	}elseif(wp_verify_nonce( $_REQUEST['token'], "wp_token" )) { 
		$post = get_post( $_REQUEST['id'] );
		setup_postdata($post);
	}	
	
	// AJAX TOKEN
	$token = wp_create_nonce("wp_token");
	
	
	$prev_post = getNextBack('prev','servicio',$post->menu_order,$post->post_date);
	$next_post = getNextBack('next','servicio',$post->menu_order,$post->post_date);

		?>	
        
<div id="singlecontent">

		<div class="columns navibg border-color">
            <div class="four columns alpha">
            	<h3><?php _e('Serveis'); ?></h3>
            </div>
            
            <div class="twelve columns omega">
            	<div class="navigate">
                	<hr class="resshow border-color" /> 
                    <span class="pname"></span>
                    <?php if(!empty( $prev_post['ID'] )) { ?>
                    <a href="<?php echo get_permalink($prev_post['ID']); ?>" data-type="servicio" data-token="<?php echo $token?>" data-id="<?php echo $prev_post['ID']?>" title="<?php echo htmlspecialchars($prev_post['post_title'])?>" class="navigate back getservicios-nextback getservicios-showmsg">&nbsp;</a>
                    <?php } ?>
                    <?php if(!empty( $next_post['ID'] )) { ?>
                    <a href="<?php echo get_permalink($next_post['ID']); ?>" data-type="servicio" data-token="<?php echo $token?>" data-id="<?php echo $next_post['ID']?>" title="<?php echo htmlspecialchars($next_post['post_title'])?>" class="navigate next getservicios-nextback getservicios-showmsg">&nbsp;</a>
					<?php } ?>
            	</div>
            </div>	
        </div>
  
	<?php
    
      $permalink = get_permalink( $post->ID );
      $thumbID = get_post_thumbnail_id( $post->ID );
      $imgDestacada = ( $thumbID ) ? wp_get_attachment_url( $thumbID ) : FALSE ;

    ?>      
       <div class="postwraps sixteen columns showajaxcontent border-color">
                      <div class="fifteensp columns offset-by-half alpha">
                      <?php if ( $imgDestacada ) { ?>
                      <img src="<?php echo $imgDestacada ?>" class="imagen_destacada_page" />
                      <?php } ?>
                           <h2 class="titles" style="text-shadow:none!important;"><a href="<?php echo $permalink ?>" style="text-shadow:none!important;"><?php the_title(); ?></a></h2>
                           <hr />
                      </div>  
                      
                      <?php 
					  	  $sharittop = of_get_option('md_social_post_disable_top');
						 if(!$sharittop) { $coln = 'twelve'; }else{ $coln = 'fifteen'; }
						  
						  $serviciodesc = '<br class="clear" /><div class="fifteensp columns offset-by-half alpha fitvids">';
						  
						  if(!$sharittop) {
						  	$serviciodesc .=  '<div class="'.$coln.' columns alpha">';
						  }
						  $serviciodesc .= apply_filters('the_content', $post->post_content);
						  
						  if(!$sharittop) {
						  	$serviciodesc .= '&nbsp;</div>';
						  }
						 
                        $serviciodesc .='</div>';
					  ?>
              	
              <br class="clear" />
              
              <div class="postcontent fitvids">
              		<?php 
						$s1=0;
						$s2=0;
						$mediacaption = unserialize(get_post_meta( $post->ID, 'servicio-media-caption', true ));
						$mediavideo = unserialize(get_post_meta( $post->ID, 'servicio-media-video', true ));
					?>
                    <br class="clear" />
		 	</div>     
            
      <div class="fifteensp columns offset-by-half alpha pinfo">
        <?php the_content(); ?> 
      </div>
            
           
                <div class="fifteensp columns offset-by-half alpha" style="margin-bottom:10px;">
                        <hr class="resshow border-color-servicios" /> 
                    <div class="navigate pull-right">
                        <span class="pname"></span> 
                    <?php if(!empty( $prev_post['ID'] )) { ?>
                    <a href="<?php echo get_permalink($prev_post['ID']); ?>" data-type="servicio" data-token="<?php echo $token?>" data-id="<?php echo $prev_post['ID']?>" title="<?php echo htmlspecialchars($prev_post['post_title'])?>" class="navigate back getservicios-nextback getservicios-showmsg">&nbsp;</a>
                    <?php } ?>
                    <?php if(!empty( $next_post['ID'] )) { ?>
                    <a href="<?php echo get_permalink($next_post['ID']); ?>" data-type="servicio" data-token="<?php echo $token?>" data-id="<?php echo $next_post['ID']?>" title="<?php echo htmlspecialchars($next_post['post_title'])?>" class="navigate next getservicios-nextback getservicios-showmsg">&nbsp;</a>
					<?php } ?>
                    </div>
                </div>
                        <br class="clear" />
                        <br class="clear" />
           
		 </div>     
  </div> 
                        
