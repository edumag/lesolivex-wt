## Detalles del tema.

El resaltado de sintaxis se consigue con el módulo syntaxhighlighter

En caso de utilizar markdown para que se muestre correctamente se debe
sustituir "```" por [bash], por ejemplo.

## Pendiente

- @todo Los idiomas no se pueden seleccionar desde formato móvil.
- @todo Formulario de contacto en castellano.
- Widgets de pie de página.
