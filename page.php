<?php
// Ruta de la imagen destacada (tamaño completo)
$thumbID = get_post_thumbnail_id( $post->ID );
$imgDestacada = ( $thumbID ) ? wp_get_attachment_url( $thumbID ) : FALSE ;
?>

<?php get_header(); ?>

	<br class="clear" />
    
	<div class="defaultpage">
  
  
	<?php if ( have_posts() ) { the_post(); ?>
      <div class="two-thirds column">
        <div class="header_page">
        <?php if ( $imgDestacada ) { ?>
        <img src="<?php echo $imgDestacada ?>" class="imagen_destacada_page" />
        <?php } ?>
        <h2 class="titles" style="text-shadow:none!important;"><?php the_title(); ?></h2>
        </div>
      <?php the_content(); ?>
    </div>
	<?php } ?>
    
        <div class="one-third column">
             <?php get_template_part( 'sidebar', 'page' ); ?>
        </div>
	</div>	
<?php get_footer(); ?>
	
